﻿using DataLayer.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataLayer.Model
{
   public class Task
    {
        [Key]
        public int TaskId { get; set; }

        public int UserId { get; set; }
        public int CategoryId { get; set; }

        [Display(Name ="عنوان")]
        [Required(ErrorMessage ="مقدار برای {0} وارد نمایید")]
        [MaxLength(200, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string Name { get; set; }

        [Display(Name = "توضیحات")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(500, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        public string Description { get; set; }

        [Display(Name = "تاریخ افزودن")]
        public DateTime CreateDate { get; set; }


        [Display(Name = "تاریخ اتمام")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        public DateTime Date { get; set; }

        [Display(Name = "انجام شد")]
        public IsDoneEnum IsDone { get; set; }

        public virtual User User { get; set; }
        public virtual Category Category { get; set; }

    }
}
