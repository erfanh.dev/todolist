﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Model
{
   public class Category
    {
        [Key]
        public int CategoryId { get; set; }

        public int UserId { get; set; }

        [Display(Name = "عنوان")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(200, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        public string Title { get; set; }

        public int? ParentId { get; set; }


        [ForeignKey("ParentId")]
        public virtual List<Category> Categories { get; set; }

        public virtual ICollection<Task> Task { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
