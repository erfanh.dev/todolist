﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.ComponentModel.DataAnnotations;
using DataLayer.Model.Enums;

namespace DataLayer.Model
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

         [Display(Name ="نام کاربری")]
         [Required(ErrorMessage ="مقدار برای {0} وارد نمایید")]
         [MaxLength(250, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string UserName { get; set; }


         [Display(Name ="ایمیل")]
         [Required(ErrorMessage ="مقدار برای {0} وارد نمایید")]
         [MaxLength(250, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
         [EmailAddress]
        public string Email { get; set; }

         [Display(Name ="کلمه عبور")]
         [Required(ErrorMessage ="مقدار برای {0} وارد نمایید")]
         [MaxLength(250, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]

        public string Password { get; set; }

        [Display(Name ="شغل")]    
        [MaxLength(50, ErrorMessage ="نباید بیشنر از {1} کاراکتر باشد")]
        public string Job { get; set; }

        [Display(Name = "تصویر کاربر")]
        [MaxLength(50, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        public string ImageName { get; set; }

        [Display(Name = "جنسیت")]
        public GenderEnum Gender { get; set; }

        [Display(Name = "سن")]
        public byte Age { get; set; }

        [Display(Name = "کدفعالسازی")]
        public string ActiveCode { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<Task> Task { get; set; }
        public virtual ICollection<Category> Category { get; set; }
    }
}
