﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Client_app.Interface;
using Client_app.ViewModels;
using DataLayer.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Client_app.Classes;


namespace Todo.Controllers
{

    public class HomeController : Controller
    {
        private IUserRepository _userRepository;
        private ITaskRepository _taskRepository;

        public HomeController(IUserRepository userRepository, ITaskRepository taskRepository)
        {
            _userRepository = userRepository;
            _taskRepository = taskRepository;
        }

        public IActionResult Index()
        {
            return View();
        }


        [Route("Account")]
        [Authorize]
        public IActionResult Account(int id)
        {
            ViewBag.categorys= _taskRepository.GetGroupTask();
            ViewBag.subcategory = _taskRepository.GetSubGroupTask(id);
            ViewBag.showtask = _taskRepository.GetAllTask(User.Identity.Name);
            ViewBag.Sidebar = _userRepository.userSidebar(User.Identity.Name);
            ViewBag.tomorrowtask = _taskRepository.GetShowTodayTask(User.Identity.Name);
            return View();
        }

        [Route("EditeProfile")]
        public IActionResult EditeProfile()
        {
            return View(_userRepository.GetEditeProfile(User.Identity.Name));
        }



        [Route("EditeProfile")]
        [HttpPost]
        public IActionResult EditeProfile(EditeUserViewModel edit, IFormFile imgU)
        {
            if (!ModelState.IsValid)
            {
                return View(edit);
            }

            _userRepository.EditPorfile(User.Identity.Name, edit, imgU);
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect("/Login?EditeProfile=true");
        }

        [Route("ChangePassword")]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [Route("ChangePassword")]
        [HttpPost]
        public IActionResult ChangePassword(ChangePasswordViewModel change)
        {
            string user = User.Identity.Name;
            if (!ModelState.IsValid)
            {
                return View(change);
            }

            if (!_userRepository.CompareOldPassword(change.oldPassword, user))
            {
                ModelState.AddModelError("oldPassword", "کلمه عبور فعلی صحیح نمیباشد");
                return View(change);
            }

            _userRepository.ChangePassword(user, change.Password);
            ViewBag.IsSuccess = true;
            return View();
        }
        [Route("Group")]
        public IActionResult Group()
        {
            return View(_taskRepository.GetAllCategory(User.Identity.Name));
        }


        [Route("AddGroup/{id?}")]
        public IActionResult AddGroup(int? id)
        {
            return View();
        }

        [Route("AddGroup/{id?}")]
        [HttpPost]
        public IActionResult AddGroup(Category category, int? id)
        {
            if (!ModelState.IsValid)
            {
                return View(category);
            }
            category.ParentId = id;
            category.UserId = _taskRepository.GetUserIdByEmail(User.Identity.Name);
            _taskRepository.AddGroup(category);
            return Redirect("/Group");
        }

        [Route("EditCategory/{id?}")]
        public IActionResult EditCategory(int id)
        {
            return View(_taskRepository.GetCategoryById(id));
        }
        [Route("EditCategory/{id?}")]
        [HttpPost]
        public IActionResult EditCategory(Category category)
        {
            if (!ModelState.IsValid)
            {
                return View(category);
            }
           
            category.UserId = _taskRepository.GetUserIdByEmail(User.Identity.Name);
            _taskRepository.UpdateGroup(category);

            return Redirect("/Group");
        }

        [Route("GetSubGroup/{id:int}")]
        public IActionResult GetSubGroup(int id)
        {
         
            List<Category> list = _taskRepository.GetSubGroupTask(id);
            return Json(list);
        }

        [Route("AddTask")]
        [HttpPost]
        public IActionResult AddTask(DataLayer.Model.Task task)
        {
            if (!ModelState.IsValid)
            {
                return View(task);
            }
            task.UserId = _taskRepository.GetUserIdByEmail(User.Identity.Name);
          
            _taskRepository.AddTask(task);
            return Redirect("/Account");
        }
    
        [Route("EditTask/{id}")]
        public IActionResult EditTask(int id)
        {
            var model = _taskRepository.EditTaskUser(id);
            if (model == null)
            {
                return NotFound();
            }
            ViewBag.categorys = _taskRepository.GetGroupTask();
            ViewBag.subcategory = _taskRepository.GetSubGroupTask(model.CategoryId);
        
            return View(model);
        }


        [Route("EditTask/{id}")]
        [HttpPost]
        public IActionResult EditTask(DataLayer.Model.Task task)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.categorys = _taskRepository.GetGroupTask();
                ViewBag.subcategory = _taskRepository.GetSubGroupTask(task.CategoryId);
                return View(task);
            }
            task.UserId= _taskRepository.GetUserIdByEmail(User.Identity.Name);
            _taskRepository.EditTask(task);
            return Redirect("/Account");
        }


        [Route("ShowAllTask")]
        public IActionResult ShowAllTask(int id ,int pageId = 1 , string filterName="")
        {
            ViewBag.categorys = _taskRepository.GetGroupTask();
            ViewBag.subcategory = _taskRepository.GetSubGroupTask(id);
            ViewBag.showtask = _taskRepository.GetAllTask(User.Identity.Name);
            return View(_taskRepository.GetShowAllTask(User.Identity.Name, pageId, filterName));
        }

        public IActionResult ShowTaskIsdone( int pageId = 1, string filterName = "")
        {
            return View(_taskRepository.GetShowIsdoneTask(User.Identity.Name, pageId, filterName));
        }

        public IActionResult ShowTaskTomorrow(int pageId = 1, string filterName = "")
        {
            return View(_taskRepository.GetShowTomorrow(User.Identity.Name, pageId, filterName));
        }

    }
}
