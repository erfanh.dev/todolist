﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Client_app;
using Client_app.Classes;
using Client_app.Interface;
using Client_app.Service;
using Client_app.ViewModels;
using DataLayer.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace Todo.Controllers
{
    public class AccountController : Controller
    {
        private IUserRepository _UserRepository;
        private IViewRenderService _viewRenderService;
        public AccountController(IUserRepository UserRepository, IViewRenderService viewRenderService)
        {
            _UserRepository = UserRepository;
            _viewRenderService = viewRenderService;
        }
        [Route("Register")]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [Route("Register")]
        public IActionResult Register(RegisterViewModel register)
        {
            if (ModelState.IsValid)
            {
                if(_UserRepository.IsUserExsite(register.Email))
                {
                    ModelState.AddModelError("Email", "ایمیل وارد شده صحیح نمی باشد.با ایمیل مورد نظر قبلا ثبت نام کردید");
                   
                    return View(register);
                }
                else if (_UserRepository.IsUserNameExsite(register.UserName))
                {
                    ModelState.AddModelError("UserName", "نام کاربری وارد شده صحیح نمی باشد.با نام کاربری مورد نظر قبلا ثبت نام کردید");
                    return View(register);
                }
                else

                {
                    User user = new User()
                    {
                        UserName = register.UserName,
                        Email = register.Email,
                        Password = HashGenerator.EncodepassWithMd5(register.Password),
                        IsActive= false,
                        ActiveCode = NameGenerator.GenerateUniqCode(),
                        ImageName = "Defult.jpg",
                    };
                    _UserRepository.AddUser(user);
                    _UserRepository.Save();

                    //string body = _viewRenderService.RenderToStringAsync("_ActiveEmail", user);
                    //SendEmail.Send(user.Email, "فعالسازی", body);

                    return RedirectToAction("Login");
                }
            }
            else
            {
                return View(register);
            }


         
        }


        [Route("Login")]
        public IActionResult Login(bool EditeProfile = false)
        {
            ViewBag.EditeProfile = EditeProfile;
            return View();
        }

        [HttpPost]
        [Route("Login")]
        public IActionResult Login(LoginViewModel login)
        {
            if (!ModelState.IsValid)
            {
                return View(login);
            }
                var user = _UserRepository.LoginUser(login);

            if (user != null)
            {
                if (user.IsActive)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                        new Claim(ClaimTypes.Name, login.Email)
                    };
                    var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    var principal = new ClaimsPrincipal(identity);
                    var properties = new AuthenticationProperties
                    {
                        IsPersistent = login.RememberMe
                    };
                    HttpContext.SignInAsync(principal, properties);


                    ViewBag.IsSuccess = true;
                    return View();
                }
                else
                {
                    ModelState.AddModelError("Email", "  حساب کاربری خود را فعال نمایید");
                }
            }
          
            else
            {
                ModelState.AddModelError("Email", "مشخصات کاربری صحیح نمی باشد");
            }
            return View(login);

        }
      
        public IActionResult ActiveAccount(string id)
        {
            ViewBag.IsActive = _UserRepository.ActiveAccount(id);
            return View();
        }

        [Route("ForgotPassword")]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [Route("ForgotPassword")]
        [HttpPost]
        public IActionResult ForgotPassword(ForgotPasswordViewModel forget)
        {
            if (!ModelState.IsValid)
            {
                return View(forget);
            }

            string fixemail = FixedText.FixEmail(forget.Email);
            User user = _UserRepository.GetUserByEmail(fixemail);

            if(user == null)
            {
                ModelState.AddModelError("Email", "کاربری یافت نشد");
            }
            string body = _viewRenderService.RenderToStringAsync("_ForgotPassword", user);
            SendEmail.Send(user.Email,"تغییرکلمه عبور",body);

            ViewBag.IsSuccess = true;

            return View();
          
        }

    
        public IActionResult ResetPassword(string id)
        {
            return View(new ResetPasswordViewModel() { 
            
                ActiveCode = id
            
            }
            );
        }


        [Route("ResetPassword")]
        [HttpPost]
        public IActionResult ResetPassword(ResetPasswordViewModel reset)
        {
            if (!ModelState.IsValid)
            {
                return View(reset);
            }

            User user = _UserRepository.GetUserByActivecode(reset.ActiveCode);
            if(user == null)
            {
                return NotFound();
            }

            string HashPassword = HashGenerator.EncodepassWithMd5(reset.Password);
            user.Password = HashPassword;
            _UserRepository.UpdateUser(user);
            return Redirect("/Login");
        }

        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect("/");
        }



    }
}
