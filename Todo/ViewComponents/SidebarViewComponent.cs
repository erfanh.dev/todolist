﻿using Client_app.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Todo.ViewComponents
{
    public class SidebarViewComponent : ViewComponent
    {
        private IUserRepository _userRepository;
        public SidebarViewComponent(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.FromResult((IViewComponentResult)View("SidebarComponent", _userRepository.GetEditeProfile(User.Identity.Name)));
        }
    }
}
