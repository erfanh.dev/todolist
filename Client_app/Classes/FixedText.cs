﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client_app.Classes
{
   public static class FixedText
    {

        public static string FixEmail(string email)
        {
            return email.Trim().ToLower();
        }
    }
}
