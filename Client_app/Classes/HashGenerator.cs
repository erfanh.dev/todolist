﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Client_app.Classes
{
  public static  class HashGenerator
    {
        public static string EncodepassWithMd5(string password)
        {
            Byte[] mainBytes;
            Byte[] encodeBytes;

            MD5 md5;
            md5 = new MD5CryptoServiceProvider();
            mainBytes = ASCIIEncoding.Default.GetBytes(password);
            encodeBytes = md5.ComputeHash(mainBytes);
            return BitConverter.ToString(encodeBytes);

        }
    }
}
