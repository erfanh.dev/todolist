﻿
using System;
using System.Collections.Generic;
using System.Text;
using Client_app.ViewModels;
using DataLayer.Model;
using Microsoft.AspNetCore.Http;

namespace Client_app.Interface
{
   public interface IUserRepository
    {
        void AddUser(User user);
        User LoginUser(LoginViewModel login);
        bool IsUserExsite(string email);
        bool IsUserNameExsite(string username);
        SidebarViewModel userSidebar(string email);
        EditeUserViewModel GetEditeProfile(string email);
        void EditPorfile(string email, EditeUserViewModel edit, IFormFile imgU);
        User GetUserByEmail(string email);
        User GetUserByActivecode(string activecode);
        void UpdateUser(User user);
        bool CompareOldPassword( string oldPassword, string email);

        bool ActiveAccount(string activecode);

      

        void ChangePassword(string email, string newPassword);


        void Save();
    }
}
