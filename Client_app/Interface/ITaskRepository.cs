﻿using Client_app.ViewModels;
using DataLayer.Model;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Client_app.Interface
{
   public interface ITaskRepository
    {

        List<Category> GetAllCategory(string email);
        void AddGroup(Category category);
        void UpdateGroup(Category category);
        int GetUserIdByEmail(string email);
        Category GetCategoryById(int id);
        List<Category> GetGroupTask();
        void AddTask(Task task);
        List<Category> GetSubGroupTask(int categoryId);
        List<Task> GetAllTask(string email);
        Task EditTaskUser(int id);
        void EditTask(Task task);
        AllTaskViewModel  GetShowAllTask(string email,int pageId= 1 , string filterName="");
        List<Task> GetShowTodayTask(string email);
        AllTaskViewModel GetShowIsdoneTask(string email, int pageId = 1, string filterName = "");

        AllTaskViewModel GetShowTomorrow(string email, int pageId = 1, string filterName = "");
    }
}
