﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Client_app.ViewModels
{
   public class LoginViewModel
    {

        [Display(Name = "ایمیل")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "کلمه عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "مرابخاطربسپار")]
        public bool RememberMe { get; set; }
    }
}
