﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using DataLayer.Model.Enums;

namespace Client_app.ViewModels
{
    public class SidebarViewModel
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string ImageName { get; set; }

    }

    public class EditeUserViewModel
    {
        [Display(Name = "نام کاربری")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        public string UserName { get; set; }


        [Display(Name = "ایمیل")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "شغل")]
        [MaxLength(50, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        public string Job { get; set; }

        [Display(Name = "تصویر کاربر")]
        [MaxLength(50, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        public string ImageName { get; set; }

        [Display(Name = "جنسیت")]
        public GenderEnum Gender { get; set; }

        [Display(Name = "سن")]
        public byte Age { get; set; }



    }

    public class ChangePasswordViewModel
    {
        [Display(Name = "کلمه عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        public string oldPassword { get; set; }

        [Display(Name = "کلمه عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "کلمه عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "رمز عبور با یکددیگر مغایرت دارند")]
        public string RePassword { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Display(Name = "ایمیل")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        [EmailAddress]
        public string Email { get; set; }
    }

    public class ResetPasswordViewModel
    {
        public string ActiveCode { get; set; }

        [Display(Name = "کلمه عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "کلمه عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "رمز عبور با یکددیگر مغایرت دارند")]
        public string RePassword { get; set; }
    }
}
