﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Client_app.ViewModels
{
   public class RegisterViewModel
    {
        [Display(Name = "نام کاربری")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        public string UserName { get; set; }


        [Display(Name = "ایمیل")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "کلمه عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "تکرار کلمه عبور")]
        [Required(ErrorMessage = "مقدار برای {0} وارد نمایید")]
        [MaxLength(250, ErrorMessage = "نباید بیشنر از {1} کاراکتر باشد")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string RePassword { get; set; }
    }
    public enum CheckUser
    {
        EmailNotValid,
        UserNameNotValid,
        EmailAndUserNameNotvalid,
        ok
    }
}
