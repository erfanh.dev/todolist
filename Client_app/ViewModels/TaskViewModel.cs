﻿using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Client_app.ViewModels
{
 public class AllTaskViewModel
    {
        public List<Task> Tasks { get; set; }
        public int CurrentPage { get; set; }
        public int CountPage { get; set; }
    }
}
