﻿using Client_app.Interface;
using DataLayer.Context;
using DataLayer.Model;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Client_app.Classes;
using Client_app.ViewModels;

namespace Client_app.Service
{
   public class TaskRepository : ITaskRepository
    {
        private TodoDatabaseContext _ctx;
        public TaskRepository(TodoDatabaseContext ctx)
        {
            _ctx = ctx;
        }
        public void AddGroup(Category category)
        {
            _ctx.Categorys.Add(category);
            _ctx.SaveChanges();
        }

        public void AddTask(Task task)
        {
            task.CreateDate = DateTime.Now;
           task.Date= task.Date.ToMiladi();
            _ctx.Tasks.Add(task);
            _ctx.SaveChanges();

        }

        public void EditTask(Task task)
        {
            _ctx.Tasks.Update(task);
            _ctx.SaveChanges();

        }

        public Task EditTaskUser(int id)
        {
            return _ctx.Tasks.FirstOrDefault(t => t.TaskId == id);
        }

        public List<Category> GetAllCategory(string email)
        {
            int userId = GetUserIdByEmail(email);
            return _ctx.Categorys.Where(c=> c.UserId == userId).Include(c=> c.Categories).ToList();
        }

        public List<Task> GetAllTask(string email)
        {
            int userId = GetUserIdByEmail(email);
            DateTime date = DateTime.Today;
            return _ctx.Tasks.Where(t => t.UserId == userId && t.Date == date && t.IsDone == DataLayer.Model.Enums.IsDoneEnum.notdone).Take(4).ToList();
        }

        public Category GetCategoryById(int id)
        {
            return _ctx.Categorys.Find(id);
        }

     

        public List<Category> GetGroupTask()
        {
            return _ctx.Categorys.Where(c => c.ParentId == null).ToList();
        }

        public AllTaskViewModel GetShowAllTask(string email, int pageId = 1, string filterName="")
        {
            IQueryable<Task> result = _ctx.Tasks;
            if (!string.IsNullOrEmpty(filterName))
            {
                result= result.Where(t => t.Name.Contains(filterName));
            }
            int userId = GetUserIdByEmail(email);
            DateTime date = DateTime.Today;

            result = result.OrderBy(t => t.UserId == userId && t.Date == date);
            
            int take = 6;
            int skip = (pageId - 1) * take;
            AllTaskViewModel list = new AllTaskViewModel();
            list.CountPage = pageId;
            list.CountPage = result.Count() / take;
            list.Tasks= result.Skip(skip).Take(take).ToList();
            return list;
        }

        public AllTaskViewModel GetShowIsdoneTask(string email, int pageId = 1, string filterName = "")
        {
            IQueryable<Task> result = _ctx.Tasks;
            if (!string.IsNullOrEmpty(filterName))
            {
                result = result.Where(t => t.Name.Contains(filterName));
            }
            int userId = GetUserIdByEmail(email);
            DateTime date = DateTime.Today;

            result = result.Where(t => t.UserId == userId && t.IsDone == DataLayer.Model.Enums.IsDoneEnum.isdone);

            int take = 6;
            int skip = (pageId - 1) * take;
            AllTaskViewModel list = new AllTaskViewModel();
            list.CountPage = pageId;
            list.CountPage = result.Count() / take;
            list.Tasks = result.Skip(skip).Take(take).ToList();
            return list;
        
         
        }

        public List<Task> GetShowTodayTask(string email)
        {
            int userId = GetUserIdByEmail(email);
            DateTime date = DateTime.Now.Date.AddDays(1).Date;
            return _ctx.Tasks.Where(t => t.UserId == userId && t.Date == date && t.IsDone == DataLayer.Model.Enums.IsDoneEnum.notdone).Take(4).ToList();
        }

        public AllTaskViewModel GetShowTomorrow(string email, int pageId = 1, string filterName = "")
        {
            IQueryable<Task> result = _ctx.Tasks;
            if (!string.IsNullOrEmpty(filterName))
            {
                result = result.Where(t => t.Name.Contains(filterName));
            }
            int userId = GetUserIdByEmail(email);
            DateTime date = DateTime.Now.AddDays(1).Date;

            result = result.Where(t => t.UserId == userId && t.Date == date);

            int take = 6;
            int skip = (pageId - 1) * take;
            AllTaskViewModel list = new AllTaskViewModel();
            list.CountPage = pageId;
            list.CountPage = result.Count() / take;
            list.Tasks = result.Skip(skip).Take(take).ToList();
            return list;
        }

        public List<Category> GetSubGroupTask(int categoryId)
        {
            return _ctx.Categorys.Where(c => c.ParentId == categoryId).ToList();
        }

     

        public int GetUserIdByEmail(string email)
        {
            return _ctx.Users.Single(u => u.Email == email).UserId;
        }

        public void UpdateGroup(Category category)
        {
            _ctx.Categorys.Update(category);
            _ctx.SaveChanges();
        }
    }
}
