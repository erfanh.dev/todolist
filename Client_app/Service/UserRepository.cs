﻿using Client_app.Classes;
using Client_app.Interface;
using Client_app.ViewModels;
using DataLayer.Context;
using DataLayer.Model;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Client_app.Service
{
    public class UserRepository : IUserRepository
    {
        private TodoDatabaseContext _ctx;
        public UserRepository(TodoDatabaseContext ctx)
        {
            _ctx = ctx;
        }
        public void AddUser(User user)
        {
            _ctx.Users.Add(user);
        }



        public User LoginUser(LoginViewModel login)
        {
            string HashPassword = HashGenerator.EncodepassWithMd5(login.Password);
            string email = FixedText.FixEmail(login.Email);
            return _ctx.Users.SingleOrDefault(u => u.Email == email && u.Password == HashPassword);
        }
        public bool IsUserExsite(string email)
        {
            return _ctx.Users.Any(u => u.Email == email);
        }

        public void Save()
        {
            _ctx.SaveChanges();
        }

        public bool IsUserNameExsite(string username)
        {
            return _ctx.Users.Any(u => u.UserName == username);
        }

        public SidebarViewModel userSidebar(string email)
        {
            return _ctx.Users.Where(u => u.Email == email).Select(u => new SidebarViewModel()
            {
                UserName = u.UserName,
                Email = u.Email,
                ImageName = u.ImageName

            }).Single();
        }

        public EditeUserViewModel GetEditeProfile(string email)
        {
            return _ctx.Users.Where(u => u.Email == email).Select(u => new EditeUserViewModel()
            {
                UserName = u.UserName,
                Email = u.Email,
                Job = u.Job,
                Age = u.Age,
                Gender = u.Gender,
                ImageName = u.ImageName

            }).Single();
        }

        public void EditPorfile(string email, EditeUserViewModel edit, IFormFile imgU)
        {
            if (imgU != null)
            {
                 string imagePath = "";
               if(edit.ImageName != "Defult.jpg")
                {
                    imagePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/UserAvatar", edit.ImageName);
                    if (File.Exists(imagePath))
                    {
                        File.Delete(imagePath);
                    }
                }

                edit.ImageName = imgU.FileName;
                string imgPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/UserImage", edit.ImageName);

                using (var stream = new FileStream(imgPath, FileMode.Create))
                {
                    imgU.CopyTo(stream);
                }

            }
            var user = GetUserByEmail(email);
            user.UserName = edit.UserName;
            user.Email = edit.Email;
            user.ImageName = edit.ImageName;
            user.Job = edit.Job;
            user.Age = edit.Age;
            user.Gender = edit.Gender;

            _ctx.Update(user);
            _ctx.SaveChanges();


        }

        public User GetUserByEmail(string email)
        {
            return _ctx.Users.SingleOrDefault(u => u.Email == email);
        }

        public bool CompareOldPassword(string oldPassword, string email)
        {
            string hashOldPassword = HashGenerator.EncodepassWithMd5(oldPassword);
            return _ctx.Users.Any(u=> u.Email == email && u.Password == hashOldPassword);
        }

        public void ChangePassword(string email, string newPassword)
        {
            var user = GetUserByEmail(email);
            user.Password = HashGenerator.EncodepassWithMd5(newPassword);
            _ctx.Update(user);
            _ctx.SaveChanges();
        }

        public bool ActiveAccount(string activecode)
        {
            var user = _ctx.Users.SingleOrDefault(u => u.ActiveCode == activecode);
            if(user == null || user.IsActive)
            {
                return false;
            }

            user.IsActive = true;
            user.ActiveCode = NameGenerator.GenerateUniqCode();
            _ctx.SaveChanges();
            return true;

        }

        public User GetUserByActivecode(string activecode)
        {
            return _ctx.Users.SingleOrDefault(u => u.ActiveCode == activecode);
        }

        public void UpdateUser(User user)
        {
            _ctx.Users.Update(user);
            _ctx.SaveChanges();

        }
    }
}
